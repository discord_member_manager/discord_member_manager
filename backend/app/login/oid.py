import os
import re

from flask_openid import OpenID


oid_manager = OpenID(fs_store_path=os.path.abspath(os.path.join(os.path.dirname(__file__), 'openid')))
steam_id_pattern = re.compile('steamcommunity.com/openid/id/(.*?)$')
