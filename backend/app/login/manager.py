from flask_jwt_extended import JWTManager

login_manager = JWTManager()


@login_manager.user_claims_loader
def add_claims_to_access_token(user):
    return {}


@login_manager.user_identity_loader
def user_identity_lookup(user):
    return user.steam_id
