import logging

from ....database.models import User


logger = logging.getLogger(__name__)


def get_user(steam_id: int = None, discord_id: int = None):
    logger.debug(f'Getting Steam ID: {steam_id}, Discord ID: {discord_id}')
    rv = User.get(steam_id, discord_id, result_required=True)
    logger.debug(f'Received User: {rv}')
    return rv


def get_users(page=1, per_page=10):
    logger.debug(f'Get User Page. Page: {page}, Per Page: {per_page}')
    rv = User.query.paginate(page, per_page)
    logger.debug(f'Retrieved {len(rv.items)} Users.')
    return rv
