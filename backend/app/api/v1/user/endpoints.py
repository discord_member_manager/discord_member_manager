from flask import request
from flask_restplus import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity

from . import ns
from .serializer import serialized_user, serialized_user_list
from .methods import get_user, get_users


@ns.route('')
class UserRoot(Resource):

    @jwt_required
    @ns.marshal_list_with(serialized_user_list,
                          mask='items{id,steam_id,discord_id,active},'
                               'page,pages,per_page,total')
    def get(self):
        return get_users(int(request.args.get('page', 1)),
                         int(request.args.get('per_page', 10)))


@ns.route('/me')
class CurrentUserItem(Resource):

    @jwt_required
    @ns.marshal_with(serialized_user,
                     mask='id,steam_id,discord_id')
    def get(self):
        return get_user(steam_id=get_jwt_identity())


@ns.route('/<int:steam_id>')
class UserItem(Resource):

    @jwt_required
    @ns.marshal_with(serialized_user,
                     mask='id,steam_id,discord_id,active')
    def get(self, steam_id):
        return get_user(steam_id)
