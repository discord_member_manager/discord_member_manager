import logging

from flask import Blueprint, redirect, request, url_for, render_template, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity, create_access_token, set_access_cookies

from ..database.models import User
from ..login import oid_manager, steam_id_pattern


logger = logging.getLogger(__name__)
web = Blueprint('web', __name__, url_prefix='/')


@web.route('')
@jwt_required
def index():
    return ''


@web.route('openid-redirects')
@oid_manager.loginhandler
def login():
    username = get_jwt_identity()
    if username:
        user = User.get(username=username, result_required=False)
        if user.is_active:
            return redirect(url_for('web.index'))
        else:
            return redirect('https://google.com')
    return oid_manager.try_login('https://steamcommunity.com/openid')


@oid_manager.after_login
def create_or_login(resp):
    match = steam_id_pattern.search(resp.identity_url)
    steam_id = match.group(1)

    user = User.get_or_create(steam_id)
    access_token = create_access_token(identity=user)
    resp = redirect(url_for('web.index'))
    set_access_cookies(resp, access_token)
    return resp
