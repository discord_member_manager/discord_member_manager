import logging

from sqlalchemy.orm.exc import NoResultFound

from . import db


logger = logging.getLogger(__name__)


class ColumnCreationDate(object):
    creation_date = db.Column(db.DateTime, default=db.func.current_timestamp(), nullable=False)


class User(db.Model, ColumnCreationDate):
    logger = logging.getLogger(__name__ + '.User')

    steam_id = db.Column(db.BigInteger, primary_key=True)
    discord_id = db.Column(db.BigInteger, unique=True, nullable=True)
    active = db.Column(db.Boolean, default=True, nullable=False)

    def __init__(self, steam_id, discord_id=None, active=True):
        self.steam_id = steam_id
        self.discord_id = discord_id
        self.active = active

    @classmethod
    def create(cls, steam_id, discord_id=None, active=True):
        cls.logger.debug(f'Creating New User... Steam ID: {steam_id}, Discord ID: {discord_id}, Active: {active}')
        rv = User(steam_id, discord_id, active)
        db.session.add(rv)
        db.session.commit()
        db.session.flush()
        cls.logger.debug(f'New user created. Steam ID: {rv.steam_id}.')
        Log.create('New User {associated_user.steam_user.username} Created!', rv.steam_id)
        return rv

    @classmethod
    def get_or_create(cls, steam_id, discord_id=None, active=True):
        rv = User.get(steam_id)
        if rv:
            return rv
        rv = User.create(steam_id, discord_id, active)
        return rv

    @classmethod
    def get(cls, steam_id: int = None, discord_id: int = None, result_required: bool = False):
        cls.logger.debug(f'Getting Steam ID: {steam_id}, Discord ID: {discord_id}')
        if steam_id is None and discord_id is None:
            return None
        query = User.query
        if steam_id:
            query = query.filter(User.steam_id == steam_id)
        if discord_id:
            query = query.filter(User.discord_id == discord_id)
        rv = query.first()
        if result_required and rv is None:
            cls.logger.warning(f'Found no User when one was required. '
                               f'Steam ID: {steam_id}, Discord ID: {discord_id}')
            raise NoResultFound(f'A User database result was required and none was found. '
                                f'Steam ID: {steam_id}, Discord ID: {discord_id}')
        cls.logger.debug(f'Received User: {rv}')
        return rv

    @property
    def is_active(self):
        return self.active

    @staticmethod
    def has_role(_):
        """
        Future Role implementations
        """
        return True


class Log(db.Model, ColumnCreationDate):
    logger = logging.getLogger(__name__ + '.Log')

    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.Text, nullable=False)
    associated_user_id = db.Column(db.BigInteger, db.ForeignKey('user.steam_id'), nullable=False)

    associated_user = db.relationship(User, backref=db.backref('logs', lazy='dynamic'), lazy='joined')

    def __init__(self, message, associated_user_id):
        self.message = message
        self.associated_user_id = associated_user_id

    @classmethod
    def create(cls, message, associated_user_id):
        cls.logger.debug(f'Creating Log: User ID: {associated_user_id}, Message: {message}')
        rv = Log(message, associated_user_id)
        db.session.add(rv)
        db.session.commit()
        db.session.flush()
        cls.logger.debug(f'Log created, ID: {rv.id}')
        return rv

    @classmethod
    def get(cls, log_id, result_required=False):
        cls.logger.debug(f'Getting Log ID: {log_id}')
        rv = Log.query.filter(Log.id == log_id).first()
        if result_required and rv is None:
            cls.logger.warning(f'Found no Log when one was required. Log ID: {log_id}')
            raise NoResultFound(f'A log database result was required and none was found. '
                                f'Log ID: {log_id}')
        cls.logger.debug(f'Received Log: {rv}')
        return rv

    @property
    def formatted(self):
        return self.message.format(**self.__dict__)
