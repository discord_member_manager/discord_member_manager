import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Axios from 'axios'
import './plugins/bootstrap-vue'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import App from './App.vue'

import Login from '@/components/Login'
import Index from '@/components/Index'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false;

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons)

Vue.prototype.$http = Axios

const store = new Vuex.Store({
  state: {
    user: null,
    requiredGames: [
      {
        steamID: 346110,
        name: 'ARK: Survival Evolved'
      }
    ]
  },
  mutations: {
    setUser (state, user) {
      state.user = user
    }
  },
  getters: {
    getUser: state => {
      return state.user
    }
  }
})

const routes = [
  {
    path: '/',
    component: Index,
  },
  {
    path: '/login',
    component: Login
  }
];

const router = new VueRouter({
  mode: 'history',
  routes: routes
});
/* eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
router.beforeEach((to, from, next) => {
  if (to.path !== '/login') {
    if (!store.getters.getUser) {
      next('/login')
    } else {
      console.log(store.state.user)
      next()
    }
  } else {
    next()
  }
})

new Vue({
  render: h => h(App),
  router: router,
  store: store
}).$mount('#app')
